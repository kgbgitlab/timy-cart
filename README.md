# Timy Cart


> Please read this short story carefully..

**<h2>What is in Timy’s Cart?</h2>**
1. Namaku Timy, Aku sangat suka mengkoleksi **Jam Tangan**, suatu ketika aku **melihat sebuah website yang menjual berbagai macam jam tangan branded**. Kemudian aku tertarik pada suatu/beberapa produk pada website tersebut dan ingin membelinya. Namun aku bingung **bagaimana caraku melakukan pemilihan produk dan menyimpannya di keranjang belanja agar kemudian aku dapat membeli jam tersebut secara online**.
2. Dari short story tersebut, anda ingin membantu timy mewujudkan hal tersebut kemudian membuat kan page sederhana dengan meng-**konversi desain** pada yang ada pada link [mockups](https://drive.google.com/open?id=1TK_Y_8M1kYezwChOstRm0erXvRPLSAfv) berikut menjadi halaman website sesuai dengan standar **RWD (Responsive Website Design)** yang terbagi menjadi **Page for Desktop dan Page for Mobile**. Anda kemudian dapat menggunakan **CSS framework Bootstrap** versi terbaru sebagai acuan layoutingnya.
3. Sebagai penyimpanan data, Anda dapat **membuat database** dengan nama “**dbase_twc_store**”, lalu kemudian membuat tabel-tabel sebagai berikut sebagai acuan data modelnya.

**Product Category**

| Field Name | Field Type |
| ------ | ------ |
| Category_id | Integer |
| Category_name | Varchar | 

**Product Brand**

| Field Name | Field Type |
| ------ | ------ |
| Brand_id | Integer |
| Brand_name | Varchar | 

**Product**

| Field Name | Field Type |
| ------ | ------ |
| Product_id | Integer |
| Category_id | Integer | 
| Product_Name | Varchar | 
| Product_status | Tiny Integer | 

**Product Detail**

| Field Name | Field Type |
| ------ | ------ |
| Product_detail_id | Integer |
| Product_id | Integer | 
| Sku_number | Varchar | 
| Description | Text | 
| Specification | Text | 
| Link_rewrite | Varchar | 

**Product Image**

| Field Name | Field Type |
| ------ | ------ |
| Product_image_id | Integer |
| Product_id | Integer | 
| Product_image_name | Varchar | 

4. Kemudian sebagai server scripting-nya Anda **membuat sistem sederhana dengan framework PHP seperti (Codeigniter/Laravel/Yii2)** agar mampu menampung data-data yang dibutuhkan untuk menampilkan katalog produk.
5. Kemudian Anda dapat **menggabungkan server scripting tersebut atau terpisah secara konsep RESTFul API dengan Front End language seperti (HTML + CSS + Vanilla JS/Vue JS/React JS)** agar website sederhana Anda makin menarik dan best perform.
6. Untuk **menampilkan detail produk** Anda memiliki ide untuk menampilkannya **dalam bentuk Popup/Modal** dan kemudian memindahkan produk tersebut ke dalam **Cart**.
7. Kemudian, **bagaimana caranya menampilkan daftar produk yang sudah di masukkan Timy ke dalam Cart dengan mengacu pada icon-icon** yang ada dalam design tersebut??

# Tech Stack
- [x] MySQL Database
- [x] PHP with Framework (Codeigniter/Laravel/Yii2)
- [x] UI Bundling (HTML + Frameworked CSS + VueJS/ReactJS/VanillaJS)